from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite3'
db = SQLAlchemy(app)


class Person(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(63), nullable=False)
    surname = db.Column(db.String(63), nullable=False)
    age = db.Column(db.Integer, nullable=False)
    job = db.Column(db.String(63), nullable=False)

    def __repr__(self) -> str:
        return f'{self.name} {self.surname}'

    def to_json(self) -> dict:
        return {
            'id': self.id,
            'name': self.name,
            'surname': self.surname,
            'age': self.age,
            'job': self.job,
        }


ornek_db: list[dict] = [
    {
        'name': 'Mahmut',
        'surname': 'Tuncer',
        'age': 99,
        'job': 'LOer'
    },
    {
        'name': 'Yıldız',
        'surname': 'Tilbe',
        'age': -20,
        'job': 'Profesyonel Deli'
    }
]


@app.route("/")
def hello_world() -> str:
    keyword = request.args.get('keyword', 'Sekai')
    return f"<h1>Hello {keyword}</h1>"


@app.route('/human/', methods=['GET', 'POST'])
def human_view():
    if request.method == "GET":
        query = Person.query.all()
        return [i.to_json() for i in query]
    if request.method == "POST":
        person = Person(
            name=request.form['name'],
            surname=request.form['surname'],
            age=request.form['age'],
            job=request.form['job'],
        )
        db.session.add(person)
        db.session.commit()
        return person.to_json()

@app.route('/human/<int:id>', methods=['GET', 'DELETE'])
def human_detail(id: int):
    match request.method:
        case 'GET':
            try:
                return Person.query.get(id).to_json()
            except Exception:
                return 'Not Found', 404
        case 'DELETE':
            try:
                obj = Person.query.get(id)
                Person.query.filter(Person.id == obj.id).delete()
                db.session.commit()
                return {'success': True}
            except Exception as e:
                return str(e.args)


def search_person(term: str) -> list[dict]:
    output = []
    for i in ornek_db:
        if term.lower() in i['name'].lower() or term.lower() in i['surname'].lower():
            output.append(i)
    return output


@app.route('/person')
def list_persons() -> list[dict]:
    search_term = request.args.get('search')
    if search_term:
        return search_person(search_term)
    return ornek_db


@app.route("/person/<int:id>")
def get_person(id: int) -> dict:
    return ornek_db[id]


db.create_all()
app.run(debug=True, use_reloader=True)
